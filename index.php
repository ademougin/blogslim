<?php
// Autoloader inclusion
require_once 'vendor/autoload.php';

// DB configuration
Configuration::config();

// Slim app creation
$app = new \Slim\Slim(array('templates.path' => 'app/view'));

// Do not forget sessions...
session_start ();

//
// Now routing definition
//

// Get routes
$app->get('/', function() use ($app) {
    $c = new AnonymousController($app);
    $c->index();
})->name('root');

$app->get('/test', function() use ($app) {
    $c = new AnonymousController($app);
    $c->yopla();
})->name('testname');

$app->get('/item/:id', function($id) use ($app) {
    $c = new AnonymousController($app);
    $c->affiche_item($id);
})->name('affitem');

$app->get('/inscription', function() use ($app) {
    $c = new AnonymousController($app);
    $c->inscrire();
})->name('inscription');

$app->get('/connexion', function() use ($app) {
    $c = new AnonymousController($app);
    $c->connecter();
})->name('connexion');

$app->get('/deconnexion', function() use ($app) {
    $c = new AnonymousController($app);
    $c->deconnecter();
})->name('deconnexion');

$app->get('/ajout_cat', function() use ($app) {
    $c = new AnonymousController($app);
    $c->page_ajout_cat();
})->name('ajout_cat');

$app->get('/affItemsByCategorie/:id', function($id) use ($app) {
    $c = new AnonymousController($app);
    $c->affiche_items_by_categorie($id);
})->name('affItemsByCategorie');

$app->get('/saisie_billet', function() use ($app) {
    $c = new AnonymousController($app);
    $c->saisie_billet();
})->name('saisie_billet');

$app->get('/all_billets',function()use ($app){
    $c = new AnonymousController($app);
    $c->all_billets();
})->name('all_billets');

// $app->get('/delete_cat', function() use ($app) {
//     $c = new AnonymousController($app);
//     $c->insert_categorie();
// })->name('delete_cat');

// Post routes
$app->post('/ajout_info', function() use ($app) {
    $c = new AnonymousController($app);
    $c->insert_billet();
});

$app->post('/ajout_membre', function() use ($app) {
    $c = new AnonymousController($app);
    $c->insert_membre();
});

$app->post('/connect_membre', function() use ($app) {
    $c = new AnonymousController($app);
    $c->connect_membre();
});

$app->post('/ajout_cat', function() use ($app) {
    $c = new AnonymousController($app);
    $c->insert_categorie();
});

// $app->post('/delete_cat', function() use ($app){
//     $c = new AnonymousController($app);
//     $c->delete_categorie();
// });

$app->post('/saisie_billet', function() use ($app) {
    $c = new AnonymousController($app);
    $c->insert_billet();
});




// Finally, generate result
$app->run();

?>