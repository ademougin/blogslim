<?php

class User extends \Illuminate\Database\Eloquent\Model {
	protected $table = 'users';
	protected $primaryKey = 'id_user';
	public $timestamps = false;
    
	static function createUser($login, $password, $password2){
		// vérifier l'existence du login dans BDD
        $user = User::where('login', $login)->get();
        
        // si login inexistant, tester conformité mot de passe avec la police
        if ($user->isEmpty()){            
            // verifier si confirmation pass ok
            if( !($password === $password2))
                return 0;
            
            // définition de la police
            $policy = new \PasswordPolicy\Policy;
            $policy->contains('lowercase', $policy->atLeast(2));
            $policy->contains('uppercase', $policy->atLeast(1));
            $policy->contains('digit', $policy->atLeast(2));
            $policy->contains('alnum', $policy->atLeast(6));
            
            // Using JavaScript
            //$js = $policy->toJavaScript(); 
            //var policy = $js;
            
            // Testing the policy
            $result = $policy->test($password);
            
            // si test ok, créer et enregistrer l'utilisateur
            if($result){
                // générer nouveau user
                $user = new User;
                $user->login = $login;
                
                // crypter password (blowfish)
                $user->password = crypt($password, '$2y$07$MongraindeSelBlogslim/U5');
                $user->save();
                
                //mettre les infos du user en session
                $_SESSION['id'] = $user->id_user;
                $_SESSION['login'] = $user->login;
                $_SESSION['droit_admin'] = $user->droit_admin;
                return $user->id_user;
            }
            else    
                return 0;
		}
		return 0;
	}

	static function connectUser($login, $password){
		$users = User::where('login', $login)->take(1)->get();
		if(!($users->isEmpty())){
			$user = $users[0];
			if (crypt($password, '$2y$07$MongraindeSelBlogslim/U5') === $user->password){
				$_SESSION['id'] = $user->id_user;
				$_SESSION['login'] = $user->login;
				$_SESSION['droit_admin'] = $user->droit_admin;
				return $user->id_user;
			}
			else
				return 0;
		}
		else
			return 0;
	}

	static function disconnectUser(){
		$_SESSION = array();
		session_destroy();
	}


	public function billets(){
		return $this->hasMany('Billets', 'id_billet');
	}

}

?>