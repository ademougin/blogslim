<?php

class Billets extends \Illuminate\Database\Eloquent\Model {
	protected $table = 'billets';
	protected $primaryKey = 'id_billet';
	public $timestamps = false;

	static function get10BilletsOrderedByDate(){
		return Billets::take(10)->orderBy('date', 'DESC')->get();
	}

	static function getAllBilletsOrderedByDate(){
		return Billets::orderBy('date', 'DESC')->get();
	}

	static function getDetailsBillets($id){
		return Billets::find($id);
	}

	static function getFromCategorie($id){
		return Billets::take(10)->where('id_categorie', $id)->orderBy('date', 'DESC')->get();
	}

	static function createBillet($titre, $body, $user, $categorie){		
		$billet = new Billets;
		$billet->titre = $titre;
		$billet->body = $body;
		$billet->id_user = $user;
		$billet->id_categorie = $categorie;
		$billet->save();
		return $billet->id_billet;
	}


	public function users(){
		return $this->belongsTo('User', 'id_user');
	}
	
	public function categories(){
		return $this->belongsTo('Categorie', 'id_categorie');
	}

}

?>