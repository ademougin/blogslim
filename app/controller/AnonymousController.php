<?php

class AnonymousController extends Controller {

	public function header() {
		$app = $this->app;
		$app->render('header.php',compact('app'));
	}

	public function banniere(){
		$this->app->render('banniere.php');
	}

	public function footer() {
		$this->app->render('footer.php');
	}

	public function left_menu(){
		$categories = Categorie::getAllCategories();
		$this->app->render('left_menu.php', compact('categories'));
	}

	public function info(){
		$this->app->render('info.php');
	}

	public function index(){
		$billets = Billets::get10BilletsOrderedByDate();
		$this->header();
		$this->banniere();
		$this->left_menu();
		$this->app->render('homepage.php');
		$this->aff_plusieurs_billets($billets);
		$this->footer();
	}
	public function aff_plusieurs_billets($billets){
		$categories = Categorie::getAllCategories();
		$this->app->render('aff_plusieurs_billets.php', compact('billets', 'categories'));
	}

	public function all_billets(){
		$billets = Billets::getAllBilletsOrderedByDate();
		$this->header();
		$this->banniere();
		$this->left_menu();
		$this->app->render('all_billets.php');
		$this->aff_plusieurs_billets($billets);
		$this->footer();
	}

	public function affiche_items_by_categorie($id){
		$categories = Categorie::getAllCategories();
		$billets = Billets::getFromCategorie($id);
		$current = Categorie::find($id);
		$this->header();
		$this->banniere();
		$this->left_menu();
		$this->app->render('aff_items_by_categorie.php', compact ('current'));
		$this->aff_plusieurs_billets($billets);
		$this->footer();
	}

	public function saisie_billet(){
		$categories = Categorie::getAllCategories();
		$this->header();
		$this->banniere();
		$this->left_menu();	
		$this->app->render('saisie_billet.php', compact('categories'));
		$this->footer();
	}

	public function affiche_item($id){
		$billet = Billets::getDetailsBillets($id);
		$this->header();
		$this->banniere();
		$this->left_menu();	
		$this->app->render('aff_item.php', compact('id', 'billet'));
		$this->footer();
	}

	public function inscrire(){
		$this->header();
		$this->banniere();
		$this->left_menu();	
		$this->info();
		$this->app->render('inscription.php');
		$this->footer();
	}

	public function connecter(){
		$this->header();
		$this->banniere();
		$this->left_menu();
		$this->app->render('connexion.php');
		$this->info();
		$this->footer();
	}

	public function deconnecter(){
		User::disconnectUser();
		$this->app->redirect($this->app->urlFor('root'));	
	}

	public function page_ajout_cat(){
		$this->header();
		$this->banniere();
		$this->left_menu();	
		$categories = Categorie::getAllCategories();
		if (isset($_SESSION["login"]) && $_SESSION['droit_admin']==1)
			$this->app->render('saisie_cat.php', compact('categories'));
		else
			$this->app->redirect($this->app->urlFor('root'));	
		$this->footer();
	}

	// insertion membre
	public function insert_membre(){
        // Lecture des champs saisis
		$login = $this->app->request->post('login');
		$password = $this->app->request->post('password');
		$password2 = $this->app->request->post('password_confirmation');
        
        // Filtrage des caracteres speciaux
        $login = filter_var($login, FILTER_SANITIZE_STRING);
        $password = filter_var($password, FILTER_SANITIZE_STRING); 
        $password2 = filter_var($password2, FILTER_SANITIZE_STRING);
        
		$ok = User::createUser($login, $password, $password2);
		// si membre déjà existant ou erreur de saisie => rediriger vers inscription
		if (!$ok){
			$this->app->flash('info', "Membre deja existant ou erreur de saisie");
			$this->app->redirect($this->app->urlFor('inscription'));}
		// sinon membre crée et rediriger vers accueil 
			else
				$this->app->redirect($this->app->urlFor('root'));
		}

	// connection membre
		public function connect_membre(){
            // Lecture des champs saisis
            $login = $this->app->request->post('login');
			$password = $this->app->request->post('password');
            
            // Filtrage des caracteres speciaux
            $login = filter_var($login, FILTER_SANITIZE_STRING);
            $password = filter_var($password, FILTER_SANITIZE_STRING);
            
			$ok = User::connectUser($login, $password);
		// si erreur de login/pass => rediriger vers connexion
			if(!$ok){
				$this->app->flash('info', "Mauvais login / mdp");
				$this->app->redirect($this->app->urlFor('connexion'));}
		// sinon membre connecté et rediriger vers accueil
				else{
					$this->app->redirect($this->app->urlFor('root'));			
				}
			}


	// Insertion nouvelle catégorie
			public function insert_categorie(){
                // Lecture des champs saisis
				$cat = $this->app->request->post('categorie');
                // Filtrage des caracteres speciaux
                $cat = filter_var($cat, FILTER_SANITIZE_STRING);
            
				$ok = Categorie::createCategorie($cat);
				$categories = Categorie::getAllCategories();
				$this->header();
				$this->banniere();
				$this->left_menu();	
				$this->app->render('saisie_cat.php', compact('ok', 'categories'));
				$this->footer();
			}

	//suppression categorie
			public function delete_categorie($id){
				$delete = Categorie::deleteCategorie($id);
			}

	// insertion nouveau billet
			public function insert_billet(){
                // Lecture des champs saisis
				$titre = $this->app->request->post('titre');
				$comment = $this->app->request->post('body');
                $categorie = $this->app->request->post('categorie');
				$user = $_SESSION['id'];	
                
                // Filtrage des caracteres speciaux
                $titre = filter_var($titre, FILTER_SANITIZE_STRING);
                $comment = filter_var($comment, FILTER_SANITIZE_STRING);
                $categorie = filter_var($categorie, FILTER_SANITIZE_STRING);
                
				Billets::createBillet($titre, $comment, $user, $categorie);
				$billets = Billets::get10BilletsOrderedByDate();
				$this->app->redirect($this->app->urlFor('root'));
			}

		}

		?>