-- phpMyAdmin SQL Dump
-- version 4.2.10
-- http://www.phpmyadmin.net
--
-- Client :  localhost:8889
-- Généré le :  Jeu 14 Mai 2015 à 21:58
-- Version du serveur :  5.5.38
-- Version de PHP :  5.6.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Base de données :  `blogslim`
--

-- --------------------------------------------------------

--
-- Structure de la table `billets`
--

CREATE TABLE `billets` (
`id_billet` int(10) NOT NULL,
  `id_categorie` int(11) DEFAULT '1',
  `titre` varchar(64) NOT NULL,
  `body` text,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `id_user` int(10) NOT NULL COMMENT 'user qui a créé le billet'
) ENGINE=MyISAM AUTO_INCREMENT=30 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `billets`
--

INSERT INTO `billets` (`id_billet`, `id_categorie`, `titre`, `body`, `date`, `id_user`) VALUES
(1, 2, 'billet test 1', 'Généralement, on utilise un texte en faux latin (le texte ne veut rien dire, il a été modifié), le Lorem ipsum ou Lipsum, qui permet donc de faire office de texte d''attente. L''avantage de le mettre en latin est que l''opérateur sait au premier coup d''oeil que la page contenant ces lignes n''est pas valide, et surtout l''attention du client n''est pas dérangée par le contenu, il demeure concentré seulement sur l''aspect graphique.', '2015-04-27 22:00:00', 1),
(4, 2, 'billet test 4', 'Ce texte a pour autre avantage d''utiliser des mots de longueur variable, essayant de simuler une occupation normale. La méthode simpliste consistant à copier-coller un court texte plusieurs fois (« ceci est un faux-texte ceci est un faux-texte ceci est un faux-texte ceci est un faux-texte ceci est un faux-texte ») a l''inconvénient de ne pas permettre une juste appréciation typographique du résultat final.', '2015-04-29 22:00:00', 1),
(3, 2, 'billet test 3', 'Il circule des centaines de versions différentes du Lorem ipsum, mais ce texte aurait originellement été tiré de l''ouvrage de Cicéron, De Finibus Bonorum et Malorum (Liber Primus, 32), texte populaire à cette époque, dont l''une des premières phrases est : « Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit... » (« Il n''existe personne qui aime la souffrance pour elle-même, ni qui la recherche ni qui la veuille pour ce qu''elle est... »).', '2015-04-29 10:12:00', 2),
(5, 2, 'billet test 5', 'Expert en utilisabilité des sites web et des logiciels, Jakob Nielsen souligne que l''une des limites de l''utilisation du faux-texte dans la conception de sites web est que ce texte n''étant jamais lu, il ne permet pas de vérifier sa lisibilité effective. La lecture à l''écran étant plus difficile, cet aspect est pourtant essentiel. Nielsen préconise donc l''utilisation de textes représentatifs plutôt que du lorem ipsum. On peut aussi faire remarquer que les formules conçues avec du faux-texte ont tendance à sous-estimer l''espace nécessaire à une titraille immédiatement intelligible, ce qui oblige les rédactions à formuler ensuite des titres simplificateurs, voire inexacts, pour ne pas dépasser l''espace imparti.', '2015-04-29 22:00:00', 2),
(6, 2, 'billet test 6', 'Contrairement à une idée répandue, le faux-texte ne donne même pas un aperçu réaliste du gris typographique, en particulier dans le cas des textes justifiés : en effet, les mots fictifs employés dans le faux-texte ne faisant évidemment pas partie des dictionnaires des logiciels de PAO, les programmes de césure ne peuvent pas effectuer leur travail habituel sur de tels textes. Par conséquent, l''interlettrage du faux-texte sera toujours quelque peu supérieur à ce qu''il aurait été avec un texte réel, qui présentera donc un aspect plus sombre et moins lisible que le faux-texte avec lequel le graphiste a effectué ses essais. Un vrai texte pose aussi des problèmes de lisibilité spécifiques (noms propres, numéros de téléphone, retours à la ligne fréquents, composition des citations en italiques, intertitres de plus de deux lignes...) qu''on n''observe jamais dans le faux-texte.', '2015-04-29 22:00:00', 2),
(7, 1, 'billet test 7', 'Even though using "lorem ipsum" often arouses curiosity due to its resemblance to classical Latin, it is not intended to have meaning. Where text is visible in a document, people tend to focus on the textual content rather than upon overall presentation, so publishers use lorem ipsum when displaying a typeface or design in order to direct the focus to presentation. "Lorem ipsum" also approximates a typical distribution of spaces in English.', '2015-04-30 09:00:00', 2),
(8, 3, 'billet test 8', 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?', '2015-04-30 11:00:00', 2),
(9, 1, 'billet test 9', 'The text is derived from Cicero''s De Finibus Bonorum et Malorum (On the Ends of Goods and Evils, or alternatively [About] The Purposes of Good and Evil ). The original passage began: Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit (Translation: "Neither is there anyone who loves grief itself since it is grief and thus wants to obtain it"). It is not known exactly when the text acquired its current standard form; it may have been as late as the 1960s. The passage was discovered by Richard McClintock, a Latin scholar who is the publications director at Hampden-Sydney College in Virginia, by searching for citings of the rarely used word ''consectetur'' in classical literature.', '2015-04-30 12:00:00', 2),
(10, 1, 'billet test 10', 'Many variations on the standard lorem ipsum text exist, some with little resemblance to the original. Other versions have additional letters - such as k, w, and z - that were uncommon or missing in the Latin language, and nonsense words such as Z.zril, takimata, and gubergren added to the original passage to achieve a distribution of letters that more closely approximates English.', '2015-04-30 13:00:00', 3),
(11, 1, 'billet test 11', 'Cicero''s first Oration against Catiline is sometimes used in type specimens: Quousque tandem abutere, Catilina, patientia nostra? Quamdiu etiam furor iste tuus nos eludet? . . .', '2015-04-30 13:00:00', 2),
(12, 1, 'billet test 12', 'The text is derived from Cicero''s De Finibus Bonorum et Malorum (On the Ends of Goods and Evils, or alternatively [About] The Purposes of Good and Evil ). The original passage began: Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit (Translation: "Neither is there anyone who loves grief itself since it is grief and thus wants to obtain it"). It is not known exactly when the text acquired its current standard form; it may have been as late as the 1960s. The passage was discovered by Richard McClintock, a Latin scholar who is the publications director at Hampden-Sydney College in Virginia, by searching for citings of the rarely used word ''consectetur'' in classical literature.', '2015-04-30 14:00:00', 2),
(13, 1, 'billet test 13', 'The text is derived from Cicero''s De Finibus Bonorum et Malorum (On the Ends of Goods and Evils, or alternatively [About] The Purposes of Good and Evil ). The original passage began: Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit (Translation: "Neither is there anyone who loves grief itself since it is grief and thus wants to obtain it"). It is not known exactly when the text acquired its current standard form; it may have been as late as the 1960s. The passage was discovered by Richard McClintock, a Latin scholar who is the publications director at Hampden-Sydney College in Virginia, by searching for citings of the rarely used word ''consectetur'' in classical literature.', '2015-05-01 07:28:00', 2),
(14, 1, 'billet test 14', '\r\nMany variations on the standard lorem ipsum text exist, some with little resemblance to the original. Other versions have additional letters - such as k, w, and z - that were uncommon or missing in the Latin language, and nonsense words such as Z.zril, takimata, and gubergren added to the original passage to achieve a distribution of letters that more closely approximates English.', '2015-05-02 11:23:00', 2),
(15, 1, 'billet test 15', 'Even though using "lorem ipsum" often arouses curiosity due to its resemblance to classical Latin, it is not intended to have meaning. Where text is visible in a document, people tend to focus on the textual content rather than upon overall presentation, so publishers use lorem ipsum when displaying a typeface or design in order to direct the focus to presentation. "Lorem ipsum" also approximates a typical distribution of spaces in English.', '2015-05-02 22:00:00', 2),
(2, 3, 'billet de test n° 2', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', '2015-04-28 22:00:00', 3),
(17, 3, 'billet test créé sur le site', 'At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat.', '2015-05-11 14:17:46', 3),
(18, 1, 'blabla', 'The most common lorem ipsum text reads as follows: Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', '2015-05-11 14:21:24', 1),
(19, 2, 'BILLET DE ALBAN', 'Il circule des centaines de versions différentes du Lorem ipsum, mais ce texte aurait originellement été tiré de l''ouvrage de Cicéron, De Finibus Bonorum et Malorum (Liber Primus, 32), texte populaire à cette époque, dont l''une des premières phrases est : « Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit... » (« Il n''existe personne qui aime la souffrance pour elle-même, ni qui la recherche ni qui la veuille pour ce qu''elle est... »).', '2015-05-11 14:51:06', 1),
(20, 3, 'Billet de test', 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?', '2015-05-11 14:51:35', 1),
(21, 9, 'billet russe', 'Но тимэам модыратиюз конжыквуюнтюр жят. Нам ут декам жплэндидэ. Ыам ельлюд ылоквюэнтиам зигнёфэрумквюы ыт, прима ыкжплььикари ыам эи. Ад примич ныморэ ыам, ку нык модюж оффекйяж, едквюэ эуежмод такематыш ат шэа. Хабымуч пэрпэтюа янтэрэсщэт ед нам.', '2015-05-14 12:23:26', 1),
(29, 8, 'allemand', 'Guten tag ', '2015-05-14 19:55:13', 1);

-- --------------------------------------------------------

--
-- Structure de la table `categories`
--

CREATE TABLE `categories` (
`id_categorie` int(11) NOT NULL,
  `nom` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

--
-- Contenu de la table `categories`
--

INSERT INTO `categories` (`id_categorie`, `nom`) VALUES
(1, 'Anglais'),
(2, 'Français'),
(3, 'Latin'),
(8, 'Allemand'),
(9, 'Russe');

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

CREATE TABLE `users` (
`id_user` int(10) NOT NULL,
  `login` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `password` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `droit_admin` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

--
-- Contenu de la table `users`
--

INSERT INTO `users` (`id_user`, `login`, `password`, `droit_admin`) VALUES
(1, 'admin', '$2y$07$MongraindeSelBlogslim.jsq6R4IeE6Cvr7S0ohKysGMPUBAW14a', 1),
(2, 'user1', '$2y$07$MongraindeSelBlogslim.uLhMeJtTEvgdFroI/b306lhXdGIxkQm', 0),
(3, 'user2', '$2y$07$MongraindeSelBlogslim.PWsvtQ.F/DyWLta/rirvVdse8NU6ZAi', 0),
(4, 'toto', '$2y$07$MongraindeSelBlogslim.cM81Kugo8A7Ro7j/lMuDUrJg35oDI8u', 0);

--
-- Index pour les tables exportées
--

--
-- Index pour la table `billets`
--
ALTER TABLE `billets`
 ADD PRIMARY KEY (`id_billet`), ADD UNIQUE KEY `id` (`id_billet`);

--
-- Index pour la table `categories`
--
ALTER TABLE `categories`
 ADD PRIMARY KEY (`id_categorie`);

--
-- Index pour la table `users`
--
ALTER TABLE `users`
 ADD PRIMARY KEY (`id_user`), ADD UNIQUE KEY `id_user` (`id_user`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `billets`
--
ALTER TABLE `billets`
MODIFY `id_billet` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=30;
--
-- AUTO_INCREMENT pour la table `categories`
--
ALTER TABLE `categories`
MODIFY `id_categorie` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT pour la table `users`
--
ALTER TABLE `users`
MODIFY `id_user` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;